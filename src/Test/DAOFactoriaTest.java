package Test;

import DAO.CRUD;
import Entidades.SedeCentral;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Factorias.DAOFactoria;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class DAOFactoriaTest {



    @Test
    public void getDAOFactoriaTest() {

        CRUD daoFactoria = DAOFactoria.getFactoria(TipoEntidades.SedeCentral, TipoPersistencia.XML);
        assertNotNull(daoFactoria);
    }


}
