package Test;


import DAO.CRUD;
import Entidades.Delegacion;
import Entidades.Persistencia;
import Entidades.SedeCentral;
import Entidades.Socio;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Exceptions.DaoConfigurationException;
import Factorias.DAOFactoria;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


public class SedeCentralTest {

    public ArrayList<Socio> listaSocios = new ArrayList<Socio>();
    public ArrayList<Delegacion> listaDelegacion = new ArrayList<Delegacion>();
    public SedeCentral sedeCentral;


    private Socio socioContructor() {
        Socio socio = new Socio("5647839L","Roman Miralles");

        return socio;
    }
    private Delegacion delegacionConstructor () {
        Delegacion delegacion = new Delegacion("MiraFlores", "Calle Valparaiso 2");

        return delegacion;
    }


    @Test
    public void crearSocioTest() {
        Socio socio = socioContructor();

        String nombre = "Roman Miralles";
        String id = "5647839L";

        Assert.assertEquals(nombre, socio.getNombre());
        Assert.assertEquals(id, socio.getIdentificador());
        Assert.assertNotNull(socio.getFechaAlta());
        Assert.assertNull(socio.getFechaBaja());
    }

    @Test
    public void crearDelegacionTest() {
        Delegacion delegacion = delegacionConstructor();

        String nombre = "MiraFlores";
        String direccion =  "Calle Valparaiso 2";

        Assert.assertEquals(nombre, delegacion.getNombre());
        Assert.assertEquals(direccion, delegacion.getDireccion());
    }

    @Test
    public void agregarSocioTest(){

           Socio socio = socioContructor();
           listaSocios.add(socio);
           Socio soc = listaSocios.get(0);

           Assert.assertEquals(socio, soc);

    }

    @Test
    public void eliminarSocioTest() {
        Socio socio = socioContructor();
        listaSocios.add(socio);
        listaSocios.remove(0);

        int valorEsperado = 0;
        int valorDevuelto = listaSocios.toArray().length;

        Assert.assertEquals(valorDevuelto,valorEsperado);

    }

    @Test
    public void actualizarSocioTest() {
        Socio socio = socioContructor();
        Socio soc = new Socio("45869949L","Pepe Morales");
        listaSocios.add(socio);
        Socio socioDevuelto1 = listaSocios.get(0);
        listaSocios.removeIf(socio1 -> socio1.equals(socio));
        listaSocios.add(soc);
        Socio socioDevuelto2 = listaSocios.get(0);

        Assert.assertNotEquals(socioDevuelto1,socioDevuelto2);

    }

    @Test
    public void agregarDelegacionTest() {
        Delegacion delegacion = delegacionConstructor();
        listaDelegacion.add(delegacion);

        Delegacion del = listaDelegacion.get(0);

        Assert.assertEquals(delegacion,del);

    }

    @Test
    public void actualizarDelegacionTest() {
        Delegacion delegacion = delegacionConstructor();
        Delegacion del = new Delegacion("Mira Rosa","Calle Pepe Rubiales 12");
        listaDelegacion.add(delegacion);
        Delegacion delegacionDevuelta1 = listaDelegacion.get(0);
        listaDelegacion.removeIf(delegacion1 -> delegacion1.equals(delegacion));
        listaDelegacion.add(del);
        Delegacion delegacionDevuelta2 = listaDelegacion.get(0);

        Assert.assertNotEquals(delegacionDevuelta1, delegacionDevuelta2);
    }

    @Test
    public void eliminarDelegacionTest() {
        Delegacion delegacion = delegacionConstructor();
        listaDelegacion.add(delegacion);
        listaDelegacion.remove(0);

        int valorEsperado = 0;
        int valorDevuelto = listaDelegacion.toArray().length;

        Assert.assertEquals(valorDevuelto,valorEsperado);

    }

    @Test
    public void actualizarSedeTest() throws JAXBException, ParserConfigurationException, IOException, SAXException, SQLException, DaoConfigurationException {

        try {
            sedeCentral = new SedeCentral("MADRID", "5869940A");
            Socio socio = socioContructor();
            Delegacion delegacion = delegacionConstructor();

            listaDelegacion.add(delegacion);
            listaSocios.add(socio);

            TipoPersistencia persistencia = Persistencia.getPersistencia();
            CRUD<SedeCentral> sedeDAO = DAOFactoria.getFactoria(TipoEntidades.SedeCentral, persistencia);
            sedeDAO.registrar(sedeCentral);

        }catch (JAXBException jaxbException) {
            System.out.println("ERR -" + jaxbException.getErrorCode());
        }




    }



}
