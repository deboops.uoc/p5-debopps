package Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlUtils {

    private void SqlUtils() {}

    public static PreparedStatement prepararConsulta
            (Connection connection, String sql, boolean devolverIdAutogenerado, Object... valores)
            throws SQLException
    {
        PreparedStatement consulta = connection.prepareStatement(sql,
                devolverIdAutogenerado ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setValores(consulta, valores);
        return consulta;
    }

    public static void setValores(PreparedStatement consulta, Object... valores)
            throws SQLException
    {
        for (int i = 0; i < valores.length; i++) {
            consulta.setObject(i + 1, valores[i]);
        }
    }
}
