package com.DeBOOPs;
import DAO.StoredProcedureSociosBaja;
import Entidades.Persistencia;
import Enums.TipoPersistencia;

import Exceptions.DaoConfigurationException;
import Gestion.GestionSedeCentral;
import Entidades.SedeCentral;

import java.sql.SQLException;


public class Main {


	public static void main(String[] args) {
		Persistencia.setPersistencia(TipoPersistencia.SQL);

		if (Persistencia.getPersistencia() == TipoPersistencia.SQL) {
			try {
				StoredProcedureSociosBaja.crearProcedimientoEliminarSociosBaja();
			} catch (SQLException | DaoConfigurationException err) {
				System.out.println("Ha ocurrido un error al crear el procedimiento almacendo de eliminación de usuarios dados de baja.");
			}
		}

		System.out.println("======================================");
		System.out.println("PROGRAMA GESTIÓN ONG - EntreCulturas");
		System.out.println("======================================");
		SedeCentral Sede = GestionSedeCentral.getSedeCentral();
		MenuAplicacion.inicio();

	}



}
