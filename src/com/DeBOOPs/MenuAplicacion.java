package com.DeBOOPs;

import DAO.StoredProcedureSociosBaja;
import Exceptions.DaoConfigurationException;
import Gestion.GestionDelegacion;
import Gestion.GestionSedeCentral;
import Gestion.GestionSocio;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;


public class MenuAplicacion {

    public static void esperarInputUsuario() {
        try {
            System.out.println("\n");
            System.out.println("Presionar una tecla para continuar ...");

            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void resetearPantalla() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void inicio() {

        Scanner teclado = new Scanner(System.in);

        String opcion = "0";
        do {
            pintarMenu();
            opcion = teclado.nextLine();
            switch (opcion) {
                case "1":
                   GestionDelegacion.registrarDelegacion();
                    break;
                case "2":
                   GestionDelegacion.editarDelegacion();
                    break;
                case "3":
                   GestionDelegacion.eliminarDelegacion();
                    break;
                case "4":
                    GestionDelegacion.listarDelegaciones();
                    break;
                case "5":
                    GestionSocio.registrarSocio();
                    break;
                case "6":
                    GestionSocio.editarSocio();
                    break;
                case "7":
                   GestionSocio.eliminarSocio();
                    break;
                case "8":
                    GestionSocio.darBajaSocio();
                    break;
                case "9":
                    GestionSocio.listarSocios();
                    break;
                case "10":
                    GestionSedeCentral.consultarSedeCentral();
                    break;
                case "11":
                    GestionSocio.ejecutarEliminarSociosBaja();
                case "0":
                    //Sale del bucle y termina el programa.
                    break;
                default:
                    System.out.println("======================================\n"
                            + "LA OPCION ELEGIDA NO ES VALIDA: LA OPCION ES 0-10\n"
                            + "VUELVA A ESCOGER UNA OPCION:\n" +
                            "======================================");
                    pintarMenu();
                    opcion = teclado.nextLine();

            }
        } while (!opcion.equals("0"));
    }

    public static void pintarMenu() {
        System.out.println("\n");
        System.out.println("===== MENU ==== ");
        System.out.println("1.- Crear delegacion ");
        System.out.println("2.- Editar delegacion ");
        System.out.println("3.- Eliminar delegacion ");
        System.out.println("4.- Listar delegaciones ");
        System.out.println("5.- Crear socio ");
        System.out.println("6.- Editar socio ");
        System.out.println("7.- Eliminar socio ");
        System.out.println("8.- Dar de Baja socio ");
        System.out.println("9.- Listar socios ");
        System.out.println("10.- Consultar Sede Central");
        System.out.println("11.- Ejecutar procedimiento de eliminación socios baja");
        System.out.println("0.- Salir ");
        System.out.println("\n");
        System.out.println("---> Escriba el numero de la opcion:");
    }

}
