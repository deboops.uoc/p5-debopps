package Factorias;

import DAO.*;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;

public abstract class DAOFactoria {

    public static CRUD getFactoria(TipoEntidades entidad, TipoPersistencia persistencia) {
        switch (entidad) {
            case Socio:
                if (persistencia == TipoPersistencia.SQL) {
                    return new SocioDAOSQLImpl();
                } else  {
                    return new SocioDAOXMLImpl();
                }
            case Delegacion:
                if (persistencia == TipoPersistencia.SQL) {
                    return new DelegacionDAOSQLImpl();
                } else  {
                    return new DelegacionDAOXMLImpl();
                }
            case SedeCentral:
                if (persistencia == TipoPersistencia.SQL) {
                    return new SedeCentralDAOSQLImpl();
                } else  {
                    return new SedeCentralDAOXMLImpl();
                }
            default:
                return null;
        }
    }

}
