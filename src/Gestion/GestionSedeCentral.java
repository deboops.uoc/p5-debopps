package Gestion;

import DAO.CRUD;
import Entidades.Persistencia;
import Entidades.SedeCentral;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Exceptions.DaoConfigurationException;
import Factorias.DAOFactoria;

import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.Scanner;


public class GestionSedeCentral {

    static TipoPersistencia persistencia = Persistencia.getPersistencia();
    static CRUD<SedeCentral> sedeDAO = DAOFactoria.getFactoria(TipoEntidades.SedeCentral, persistencia);

    public static SedeCentral getSedeCentral() {
        SedeCentral sede = null;
        try {
            sede = sedeDAO.leerPorId("1");

        } catch(SQLException | DaoConfigurationException | JAXBException err) {
            sede = null;
        }

        if (sede == null || sede.getCodigo() == 0) {
            System.out.println("No existe Sede central, antes de seguir, debe indicar los datos para crear una: ");
            sede = crearSedeCentral();
            try {
                sedeDAO.registrar(sede);

            } catch(JAXBException | SQLException | DaoConfigurationException err) {
                System.out.println("Error - " + err.getMessage());
            }
        }

        return sede;
    }


    public static SedeCentral crearSedeCentral() {
        Scanner leetextos = new Scanner(System.in);

        System.out.println("Nombre de la sede");
        String nombre = leetextos.nextLine();
        System.out.println("CIF de la sede: ");
        String cif = leetextos.nextLine();
        return new SedeCentral(nombre, cif);
    }

    public static void consultarSedeCentral() {

        SedeCentral misede = GestionSedeCentral.getSedeCentral();
        System.out.println(misede);
    }

}
