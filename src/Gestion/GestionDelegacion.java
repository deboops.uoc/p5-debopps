package Gestion;

import DAO.CRUD;
import Entidades.Delegacion;
import Entidades.Persistencia;
import Entidades.SedeCentral;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Exceptions.DaoConfigurationException;
import Exceptions.EntidadExistenteException;
import Exceptions.NoExisteEntidadException;
import Factorias.DAOFactoria;
import com.DeBOOPs.MenuAplicacion;

import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.Scanner;

public class GestionDelegacion {

    static TipoPersistencia persistencia = Persistencia.getPersistencia();
    static CRUD<Delegacion> delegacionDAO = DAOFactoria.getFactoria(TipoEntidades.Delegacion, persistencia);

    public static void registrarDelegacion() {

        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedeCentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("=========================================");
        System.out.println("Ingresar los datos de la nueva delegación");
        System.out.println("=========================================");
        System.out.println("Nombre de la delegacion: ");
        String nombre = leetextos.nextLine();
        System.out.println("Calle de la delegacion: ");
        String calle = leetextos.nextLine();

        Delegacion delegacion = new Delegacion(nombre,calle);

        try {
            delegacionDAO.registrar(delegacion);

            // Solo aplicaba para el caso de XML
            // gestionSedeCentral.agregarDelegacion(delegacion);

            System.out.println("\n");
            System.out.println("Se ha registrado la delegación correctamente.");

        } catch (JAXBException | EntidadExistenteException | SQLException err) {
            System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

    public static void editarDelegacion() {
        listarDelegaciones();

        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedeCentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("\n===============================================");
        System.out.println("Ingresar los datos de la delegación a modificar");
        System.out.println("===============================================");
        System.out.println("Nombre de la delegación a modificar: ");
        String nombre = leetextos.nextLine();

        // Recupera la delegación a modificar
        try {
            Delegacion delegacionAModificar = delegacionDAO.leerPorId(nombre);

            if (delegacionAModificar != null) {
                System.out.println("Calle de la delegacion: ");
                delegacionAModificar.setDireccion(leetextos.nextLine());
                delegacionDAO.modificar(delegacionAModificar);

                // Solo aplicaba para el caso de XML
                // gestionSedeCentral.actualizarDelegacion(delegacionAModificar);

                System.out.println("\n");
                System.out.println("Se ha editado la delegación correctamente.");
            } else {
                System.out.println("Error - Delegación no encontrada");
            }
        } catch(JAXBException | NoExisteEntidadException | SQLException err) {
            System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();

    }

    public static void eliminarDelegacion() {
        listarDelegaciones();

        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedecentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("Nombre de la delegación a eliminar: ");
        String id = leetextos.nextLine();

        try {
            Delegacion delegacionAEliminar = delegacionDAO.leerPorId(id);

            if (delegacionAEliminar != null) {
                delegacionDAO.eliminar(delegacionAEliminar);

                // Solo aplicaba para el caso de XML
                // gestionSedecentral.eliminarDelegacion(delegacionAEliminar);

                System.out.println("\n");
                System.out.println("Se ha eliminado la delegación correctamente.");
            } else {
                System.out.println("Error - Delegación no encontrada");
            }
        } catch (JAXBException | NoExisteEntidadException | SQLException err) {
            System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

    public static void listarDelegaciones() {
        System.out.println("=======================");
        System.out.println("Listado de delegaciones");
        System.out.println("=======================");

        try {
            for(Delegacion d : delegacionDAO.listarTodos()) {
                System.out.println(d);
            }
        } catch (JAXBException | SQLException | DaoConfigurationException err) {
            System.out.println("Error - " + err.getMessage());
            MenuAplicacion.esperarInputUsuario();
            MenuAplicacion.resetearPantalla();
        }
    }
}
