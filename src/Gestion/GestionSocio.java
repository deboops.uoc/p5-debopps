package Gestion;

import DAO.CRUD;
import DAO.SocioDAO;
import DAO.StoredProcedureSociosBaja;
import Entidades.Persistencia;
import Entidades.SedeCentral;
import Entidades.Socio;
import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Exceptions.DaoConfigurationException;
import Exceptions.NoExisteEntidadException;
import Factorias.DAOFactoria;
import com.DeBOOPs.MenuAplicacion;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.Scanner;


public class GestionSocio {

    static TipoPersistencia persistencia = TipoPersistencia.SQL;
    static CRUD<Socio> socioDAO = DAOFactoria.getFactoria(TipoEntidades.Socio, persistencia);
    static Exception err;


    public static void registrarSocio() {

        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedeCentral;
        // gestionSedeCentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);
        System.out.println("===================================");
        System.out.println("Ingresar los datos del nuevo socio");
        System.out.println("===================================");

        System.out.println("Identificador del socio (NIF/NIE/CIF): ");
        String id = leetextos.nextLine();
        System.out.println("Nombre del socio: ");
        String nombre = leetextos.nextLine();
        Socio socioNuevo = new Socio(id, nombre);


        try {
            socioDAO.registrar(socioNuevo);

            // Solo aplicaba para el caso de XML
            // gestionSedeCentral.agregarSocio(socioNuevo);

            System.out.println("\n");
            System.out.println("Se ha registrado el socio correctamente.");

        } catch (JAXBException | NullPointerException | SQLException | DaoConfigurationException err) {
            System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

    public static void editarSocio() {
        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedeCentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("=========================================");
        System.out.println("Ingresar los datos del socio a modificar");
        System.out.println("=========================================");
        System.out.println("Identificador del socio a modificar (NIF/NIE/CIF): ");
        String id = leetextos.nextLine();

        try {
            // Recupera el socio a modificar
            Socio socioAModificar = socioDAO.leerPorId(id);

            if (socioAModificar != null) {
                System.out.println("Nombre del socio: ");
                socioAModificar.setNombre(leetextos.nextLine());
                socioDAO.modificar(socioAModificar);

                // Solo aplicaba para el caso de XML
                // gestionSedeCentral.actualizarSocio(socioAModificar);

                System.out.println("\n");
                System.out.println("Se ha editado el socio correctamente.");
            } else {
                System.out.println("Error - Socio no encontrado");
            }
        } catch (JAXBException | NoExisteEntidadException | DaoConfigurationException | SQLException err) {
            System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

    public static void darBajaSocio() {
        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedeCentral;
        // gestionSedeCentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("\nIdentificador del socio a dar de baja(NIF/NIE/CIF): ");
        String id = leetextos.nextLine();

        try {
            Socio socioDarBaja = socioDAO.leerPorId(id);
            if (socioDarBaja != null) {
                socioDarBaja.asignarFechaLocalAFechaBaja();
                socioDAO.modificar(socioDarBaja);

                // Solo aplicaba para el caso de XML
                // gestionSedeCentral.actualizarSocio(socioDarBaja);

                System.out.println("Se ha añadido correctamente la fecha de baja");
            } else {
                System.out.println("Error - socio no encontrado");
            }
        } catch (Exception e) {
            System.out.println("error -" + e.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

    public static void eliminarSocio() {
        // Solo aplicaba para el caso de XML
        // SedeCentral gestionSedecentral;
        // gestionSedecentral = GestionSedeCentral.getSedeCentral();

        Scanner leetextos = new Scanner(System.in);

        System.out.println("\nIdentificador del socio a eliminar(NIF/NIE/CIF): ");
        String id = leetextos.nextLine();

        try {
            Socio socioAEliminar = socioDAO.leerPorId(id);
            if (socioAEliminar != null && socioAEliminar.getFechaBaja() != null) {
                socioDAO.eliminar(socioAEliminar);
                System.out.println("\n");
                System.out.println("Se ha eliminado el socio correctamente.");
            } else {
                System.out.println("Error - Socio no encontrado o Socio todavía en alta");
            }
        } catch (JAXBException | NoExisteEntidadException | SQLException | DaoConfigurationException  err) {
                System.out.println("Error - " + err.getMessage());
        }

        MenuAplicacion.esperarInputUsuario();
        MenuAplicacion.resetearPantalla();
    }

        public static void listarSocios() {
            System.out.println("=================");
            System.out.println("Listado de socios");
            System.out.println("=================");

            try {
                for (Socio s : socioDAO.listarTodos()) {
                    System.out.println(s);
                }
            } catch (JAXBException | NoExisteEntidadException | SQLException | DaoConfigurationException err) {
                System.out.println("Error - " + err.getMessage());
                MenuAplicacion.esperarInputUsuario();
                MenuAplicacion.resetearPantalla();
            }
        }

        public static void ejecutarEliminarSociosBaja() {
            try {
                StoredProcedureSociosBaja.eliminarSociosBaja();
            } catch (SQLException | DaoConfigurationException err) {
                System.out.printf("Ha ocurrido un error en la ejecución del procedimiento de eliminación masiva de usuarios dados de baja.");
                MenuAplicacion.esperarInputUsuario();
                MenuAplicacion.resetearPantalla();
            }
        }

    }
