package DAO;

import Entidades.SedeCentral;
import Entidades.Socio;
import Exceptions.DaoConfigurationException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SedeCentralDAOSQLImpl implements SedeCentralDAO {

    private Connection conector = null;

    @Override
    public List<SedeCentral> listarTodos() throws SQLException, DaoConfigurationException {
        final String CONSULTA = "SELECT * FROM SedeCentral";

        List<SedeCentral> lista = new ArrayList<>();

        int idCodigo = 0;
        String nombre = null;
        String cif = null;
        Statement sentencia = null;

        conector = JdbcConnector.getConnection();

        try {
            sentencia = conector.createStatement();
            ResultSet rs = sentencia.executeQuery(CONSULTA);

            while (rs.next()) {
                idCodigo = rs.getInt("codigo");
                nombre = rs.getString("nombre");
                cif = rs.getString("CIF");
                SedeCentral sedeCentral = new SedeCentral(idCodigo, nombre, cif);
                lista.add(sedeCentral);
            }
            rs.close();

        } finally {
            if (sentencia != null) {
                conector.close();
            }
        }

        return lista;
    }

    @Override
    public SedeCentral leerPorId(String id) throws SQLException, DaoConfigurationException{
        final String CONSULTA = "SELECT * FROM SedeCentral";

        int idCodigo = 0;
        String nombre = null;
        String cif = null;
        PreparedStatement sentencia = null;

        conector = JdbcConnector.getConnection();

        try {
            sentencia = conector.prepareStatement(CONSULTA, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = sentencia.executeQuery();

            while (rs.next()) {
                idCodigo = rs.getInt("codigo");
                nombre = rs.getString("nombre");
                cif = rs.getString("CIF");
            }

            rs.close();

        } finally {
            if (sentencia != null) {
                conector.close();
            }
        }
        return new SedeCentral(idCodigo, nombre, cif);
    }

    @Override
    public void modificar(SedeCentral s) throws SQLException, DaoConfigurationException {
        conector = JdbcConnector.getConnection();
        // No se implementa, caso de uso no permitido
    }

    public void registrar(SedeCentral sede) throws SQLException, DaoConfigurationException {
        final String QUERY = "INSERT INTO SedeCentral(nombre, cif) VALUES(?, ?)";

        PreparedStatement sentencia = null;

        String sedeNombre = sede.getNombre();
        String sedeCif = sede.getCif();

        conector = JdbcConnector.getConnection();
        conector.setAutoCommit(false);

        try {
            sentencia = conector.prepareStatement(QUERY, Statement.NO_GENERATED_KEYS);
            sentencia.setString(1, sedeNombre);
            sentencia.setString(2, sedeCif);
            sentencia.executeUpdate();
            conector.commit();

        } catch (SQLException err) {
            conector.rollback();
            throw err;
        } finally {
            if (sentencia != null) {
                conector.close();
            }
        }
    }


    @Override
    public void eliminar(SedeCentral s) throws SQLException, DaoConfigurationException {
        conector = JdbcConnector.getConnection();
        // No se implementa, caso de uso no permitido
    }
}