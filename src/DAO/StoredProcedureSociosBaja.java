package DAO;

import Exceptions.DaoConfigurationException;

import java.sql.*;

public class StoredProcedureSociosBaja {

    private static Connection conector = null;

    public static void crearProcedimientoEliminarSociosBaja() throws SQLException {

        final String sqlBorrarProcedimiento = "DROP PROCEDURE IF EXISTS ELIMINAR_SOCIOS_BAJA";

        final String sqlCrearProcedimiento =
                "create procedure ELIMINAR_SOCIOS_BAJA() " +
                        "begin " +
                        "DELETE from Socio WHERE fechaBaja IS NOT NULL;" +
                        "end";

        conector = JdbcConnector.getConnection();
        conector.setAutoCommit(false);

        try (Statement sentenciaEliminarProcedimiento = conector.createStatement()) {
            System.out.println("Calling DROP PROCEDURE");
            sentenciaEliminarProcedimiento.execute(sqlBorrarProcedimiento);
        }

        try (Statement sentenciaCrearProcedimiento = conector.createStatement()) {
            sentenciaCrearProcedimiento.executeUpdate(sqlCrearProcedimiento);
            conector.commit();

        } catch (SQLException err) {
            conector.rollback();
            throw err;
        } finally {
            conector.close();
        }
    }


    public static void eliminarSociosBaja() throws DaoConfigurationException, SQLException {

        try {
            conector = JdbcConnector.getConnection();
            conector.setAutoCommit(false);

            CallableStatement cs = conector.prepareCall("{call ELIMINAR_SOCIOS_BAJA()}");
            int sociosEliminados = cs.executeUpdate();

            if (sociosEliminados == 0) {
                System.out.println("Procedimiento eliminar socios baja - No hay socios a eliminar.");
            } else {
                System.out.println("Procedimiento eliminar socios baja - Socios eliminados: " + sociosEliminados);
            }

            conector.commit();
        } catch (SQLException err) {
            conector.rollback();
            throw err;
        } finally {
            conector.close();
        }
    }
}
