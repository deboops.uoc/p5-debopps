package DAO;

import Colecciones.ColeccionDeDelegaciones;
import Entidades.Delegacion;
import Exceptions.EntidadExistenteException;
import Exceptions.NoExisteEntidadException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class DelegacionDAOXMLImpl implements DelegacionDAO {

    @Override
    public List<Delegacion> listarTodos() throws JAXBException {
		ColeccionDeDelegaciones coleccion = recuperarDataDeXML();
		return coleccion.getListaDelegaciones();
    }

    @Override
    public Delegacion leerPorId(String id) throws JAXBException {
        List<Delegacion> listaDelegaciones = this.listarTodos();
        return encontrarDelegacionPorNombre(id, listaDelegaciones);
    }

    @Override
    public void registrar(Delegacion nuevaDelegacion) throws JAXBException, EntidadExistenteException {
        List<Delegacion> listaDelegaciones = null;
        try {
            listaDelegaciones = this.listarTodos();
        } catch (JAXBException err) {
            listaDelegaciones = new ArrayList<>();
        }

        if (isExists(nuevaDelegacion, listaDelegaciones)) {
            throw new EntidadExistenteException("Error - La delegación con nombre " + nuevaDelegacion.getNombre() + " ya existe");
        } else {
            listaDelegaciones.add(nuevaDelegacion);
            this.guardarXML(listaDelegaciones);
        }
    }

    @Override
    public void modificar(Delegacion delegacionAModificar) throws JAXBException, NoExisteEntidadException {
        List<Delegacion> listaDelegaciones = null;
        try {
            listaDelegaciones = this.listarTodos();
        } catch (JAXBException err) {
            listaDelegaciones = new ArrayList<>();
        }

        if (!isExists(delegacionAModificar, listaDelegaciones)) {
            throw new NoExisteEntidadException("Error - La delegación con nombre " + delegacionAModificar.getNombre() + " no existe");
        } else {
            listaDelegaciones.removeIf(del -> del.equals(delegacionAModificar));
            listaDelegaciones.add(delegacionAModificar);
            this.guardarXML(listaDelegaciones);
        }

    }

    @Override
    public void eliminar(Delegacion delegacionAEliminar) throws JAXBException, NoExisteEntidadException {
        List<Delegacion> listaDelegaciones = null;
        try {
            listaDelegaciones = this.listarTodos();
        } catch (JAXBException err) {
            listaDelegaciones = new ArrayList<>();
        }

        if (!isExists(delegacionAEliminar, listaDelegaciones)) {
            throw new NoExisteEntidadException("Error - La delegación con nombre " + delegacionAEliminar.getNombre() + " no existe");
        } else {
            listaDelegaciones.removeIf(del -> del.equals(delegacionAEliminar));
            this.guardarXML(listaDelegaciones);
        }

    }

	private boolean isExists(Delegacion d, List<Delegacion> listD) {
        Delegacion encontrado = listD.stream()
                .filter(delegacion -> d.compareTo(delegacion) == 0)
                .findAny()
                .orElse(null);

        return encontrado instanceof Delegacion;
    }

    private Delegacion encontrarDelegacionPorNombre(String nombre, List<Delegacion> listD) {
        return listD.stream()
                .filter(delegacion -> delegacion.getNombre().compareTo(nombre) == 0)
                .findAny()
                .orElse(null);
    }

    private void guardarXML(List<Delegacion> listaDelegaciones) throws JAXBException {
        File referenciaFicheroFinal = new File("COLECCION_DELEGACIONES.xml");

        ColeccionDeDelegaciones coleccion = new ColeccionDeDelegaciones(listaDelegaciones);

        try {
            JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(ColeccionDeDelegaciones.class);
            Marshaller traductorDeXML = selectorDeLaClaseAExportar.createMarshaller();
            traductorDeXML.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            traductorDeXML.marshal(coleccion, referenciaFicheroFinal);

        } catch (JAXBException e) {
            throw e;
        }
    }

    private ColeccionDeDelegaciones recuperarDataDeXML() throws JAXBException {
        File referenciaFicheroFinal = new File("COLECCION_DELEGACIONES.xml");

        try {
            JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(ColeccionDeDelegaciones.class);
            Unmarshaller miUnmarshaller = selectorDeLaClaseAExportar.createUnmarshaller();
            return (ColeccionDeDelegaciones) miUnmarshaller.unmarshal(referenciaFicheroFinal);
        } catch (JAXBException e) {
            throw e;
        }
    }


}
