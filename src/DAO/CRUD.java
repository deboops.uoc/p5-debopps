package DAO;

import java.sql.SQLException;
import java.util.List;
import Exceptions.DaoConfigurationException;
import javax.xml.bind.JAXBException;

public interface CRUD<T> {

    List<T> listarTodos() throws JAXBException, SQLException, DaoConfigurationException;
    T leerPorId(String id) throws JAXBException, SQLException, DaoConfigurationException;
    void registrar(T t) throws JAXBException, SQLException, DaoConfigurationException;
    void modificar(T t) throws JAXBException, SQLException, DaoConfigurationException;
    void eliminar(T t) throws JAXBException, SQLException, DaoConfigurationException;



}
