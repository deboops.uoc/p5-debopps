package DAO;


import Colecciones.ColeccionDeSocios;
import Entidades.Socio;
import Exceptions.EntidadExistenteException;
import Exceptions.NoExisteEntidadException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class SocioDAOXMLImpl implements SocioDAO {
    @Override
    public List<Socio> listarTodos() throws JAXBException {

    	ColeccionDeSocios coleccion = recuperarDataDeXML();
    	return coleccion.getListasocios();
    }

    @Override
    public Socio leerPorId(String id) throws JAXBException {
        List<Socio> listaSocios = this.listarTodos();
        return encontrarSocioPorId(id, listaSocios);
    }

    @Override
    public void registrar(Socio nuevoSocio) throws JAXBException, EntidadExistenteException {

        List<Socio> listaSocios = null;

        try {
            listaSocios = this.listarTodos();
        } catch (JAXBException err) {
            listaSocios = new ArrayList<>();
        }

        if (isExists(nuevoSocio, listaSocios)) {
            throw new EntidadExistenteException("Error - El socio con identificador " + nuevoSocio.getIdentificador() + " ya existe");
        } else {
            listaSocios.add(nuevoSocio);
            this.guardarXML(listaSocios);
        }
    }

    @Override
    public void modificar(Socio socioAModificar) throws JAXBException, NoExisteEntidadException {
        List<Socio> listaSocios = null;

        try {
            listaSocios = this.listarTodos();
        } catch (JAXBException err) {
            listaSocios = new ArrayList<>();
        }

        if (!isExists(socioAModificar, listaSocios)) {
            throw new NoExisteEntidadException("Error - El socio con identificador " + socioAModificar.getIdentificador() + " no existe");
        } else {
            listaSocios.removeIf(soc -> soc.equals(socioAModificar));
            listaSocios.add(socioAModificar);
            this.guardarXML(listaSocios);
        }
    }

    @Override
    public void eliminar(Socio socioAEliminar) throws JAXBException, NoExisteEntidadException {
        List<Socio> listaSocios = null;

        try {
            listaSocios = this.listarTodos();
        } catch (JAXBException err) {
            listaSocios = new ArrayList<>();
        }

        if (!isExists(socioAEliminar, listaSocios)) {
            throw new NoExisteEntidadException("Error - El socio con identificador " + socioAEliminar.getIdentificador() + " no existe");
        } else {
            listaSocios.removeIf(soc -> soc.equals(socioAEliminar));
            this.guardarXML(listaSocios);
        }
    }

    private boolean isExists(Socio s, List<Socio> listS) {
        Socio encontrado = listS.stream()
                .filter(delegacion -> s.compareTo(delegacion) == 0)
                .findAny()
                .orElse(null);

        return encontrado instanceof Socio;
    }

    private Socio encontrarSocioPorId(String id, List<Socio> listS) {
        return listS.stream()
                .filter(socio -> socio.getIdentificador().compareTo(id) == 0)
                .findAny()
                .orElse(null);
    }

    private void guardarXML(List<Socio> listaSocios) throws JAXBException {
        try {
            File referenciaFicheroFinal = new File("COLECCION_SOCIOS.xml");

            ColeccionDeSocios coleccion = new ColeccionDeSocios(listaSocios);

            JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(ColeccionDeSocios.class);
            Marshaller traductorDeXML = selectorDeLaClaseAExportar.createMarshaller();
            traductorDeXML.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            traductorDeXML.marshal(coleccion, referenciaFicheroFinal);

        } catch (JAXBException e) {
            throw e;
        }
    }

    private ColeccionDeSocios recuperarDataDeXML() throws JAXBException {
        File referenciaFicheroFinal = new File("COLECCION_SOCIOS.xml");

        try {
            JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(ColeccionDeSocios.class);
            Unmarshaller miUnmarshaller = selectorDeLaClaseAExportar.createUnmarshaller();
            return (ColeccionDeSocios) miUnmarshaller.unmarshal(referenciaFicheroFinal);
        } catch (JAXBException e) {
            throw e;
        }
    }
}
