package DAO;

import Entidades.Delegacion;
import Exceptions.DaoConfigurationException;
import Utils.SqlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DelegacionDAOSQLImpl implements DelegacionDAO {

	private static final String SQL_LISTAR_TODOS =
			"SELECT codigo, nombre, direccion FROM Delegacion";
    private static final String SQL_LEER_POR_ID =
            "SELECT codigo, nombre, direccion FROM Delegacion WHERE nombre = ?";
    private static final String SQL_REGISTRAR =
            "INSERT INTO Delegacion (nombre, direccion) VALUES (?, ?)";
    private static final String SQL_MODIFICAR =
            "UPDATE Delegacion SET direccion = ? WHERE nombre = ?";
    private static final String SQL_ELIMINAR =
            "DELETE FROM Delegacion WHERE nombre = ?";

	@Override
	public List<Delegacion> listarTodos()  throws SQLException, DaoConfigurationException {

		List<Delegacion> lista = new ArrayList<>();

		Connection conector = JdbcConnector.getConnection();
		PreparedStatement consulta = conector.prepareStatement(SQL_LISTAR_TODOS);

		try {
			ResultSet resultSet = consulta.executeQuery();

			while (resultSet.next()) {
				lista.add(map(resultSet));
			}
		} finally {
			conector.close();
		}
		return lista;
	}

	@Override
	public Delegacion leerPorId(String id)  throws SQLException, DaoConfigurationException {

		Delegacion delegacion = null;

		Connection conector = JdbcConnector.getConnection();
		PreparedStatement consulta = SqlUtils.prepararConsulta(conector, SQL_LEER_POR_ID, true, id);

		try {
			ResultSet resultSet = consulta.executeQuery();

			if (resultSet.next()) {
				delegacion = map(resultSet);
			}

		} finally {
			conector.close();
		}

		return delegacion;
	}

	@Override
	public void registrar(Delegacion delegacion)  throws SQLException, DaoConfigurationException {

		Object[] valores = {
				delegacion.getNombre(),
				delegacion.getDireccion()
		};

		Connection conector = JdbcConnector.getConnection();
		conector.setAutoCommit(false);

		PreparedStatement consulta = SqlUtils.prepararConsulta(conector, SQL_REGISTRAR, true, valores);

		try {
			int nuevasFilas = consulta.executeUpdate();

			if (nuevasFilas == 0) {
				throw new SQLException("Error - No se ha registrado la delegación.");
			}

			ResultSet clavesGeneradas = consulta.getGeneratedKeys();

			if (clavesGeneradas.next()) {
				delegacion.setCodigo(clavesGeneradas.getInt(1));
			} else {
				throw new SQLException("Error - No se ha registrado la delegación.");
			}
			conector.commit();

		} catch (SQLException err) {
			conector.rollback();
			throw err;
		} finally {
			conector.close();
		}
	}

	@Override
	public void modificar(Delegacion delegacion)  throws SQLException, DaoConfigurationException {

		Object[] valores = {
				delegacion.getDireccion(),
				delegacion.getNombre()
		};

		Connection conector = JdbcConnector.getConnection();
		conector.setAutoCommit(false);

		PreparedStatement consulta = SqlUtils.prepararConsulta(conector, SQL_MODIFICAR, false, valores);

		try {
			int filasAfectadas = consulta.executeUpdate();
			if (filasAfectadas == 0) {
				throw new SQLException("Error - No se ha modificado ningún registro.");
			}
			conector.commit();

		} catch (SQLException err) {
			conector.rollback();
			throw err;
		} finally {
			conector.close();
		}
	}

	@Override
    public void eliminar(Delegacion delegacion)  throws SQLException, DaoConfigurationException {

		Object[] valores = {
				delegacion.getNombre()
		};

		Connection conector = JdbcConnector.getConnection();
		conector.setAutoCommit(false);

		PreparedStatement consulta = SqlUtils.prepararConsulta(conector, SQL_ELIMINAR, false, valores);

		try {
			int filasAfectadas = consulta.executeUpdate();
			if (filasAfectadas == 0) {
				throw new SQLException("Error - No se ha eliminado ningún registro.");
			}
			conector.commit();

		} catch (SQLException err) {
			conector.rollback();
			throw err;
		} finally {
			conector.close();
		}
    }

	private static Delegacion map(ResultSet resultSet) throws SQLException {
		Delegacion delegacion = new Delegacion();
		delegacion.setCodigo(resultSet.getInt("codigo"));
		delegacion.setNombre(resultSet.getString("nombre"));
		delegacion.setDireccion(resultSet.getString("direccion"));

		return delegacion;
	}
}
