package DAO;

import Entidades.SedeCentral;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class SedeCentralDAOXMLImpl implements SedeCentralDAO {



	@Override
	public List<SedeCentral> listarTodos() {
		return null;
	}

	@Override
	public SedeCentral leerPorId(String id) throws JAXBException {
		return recuperarDataDeXML();

	}

	@Override
	public void registrar(SedeCentral sede) throws JAXBException {
		guardarXML(sede);
	}

	@Override
	public void modificar(SedeCentral s) {
	}

	@Override
	public void eliminar(SedeCentral s) {
	}

	private void guardarXML(SedeCentral sedeCentral) throws JAXBException {
		File referenciaFicheroFinal = new File("SEDE_CENTRAL.xml");

		try {

			JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(SedeCentral.class);
			Marshaller traductorDeXML = selectorDeLaClaseAExportar.createMarshaller();
			traductorDeXML.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			traductorDeXML.marshal(sedeCentral, referenciaFicheroFinal);

		} catch (JAXBException e) {
			throw e;
		}
	}

	private SedeCentral recuperarDataDeXML() throws JAXBException {
		File referenciaFicheroFinal = new File("SEDE_CENTRAL.xml");

		try {
			JAXBContext selectorDeLaClaseAExportar = JAXBContext.newInstance(SedeCentral.class);
			Unmarshaller miUnmarshaller = selectorDeLaClaseAExportar.createUnmarshaller();
			return (SedeCentral) miUnmarshaller.unmarshal(referenciaFicheroFinal);

		} catch (JAXBException e) {
			throw e;
		}
	}
}
