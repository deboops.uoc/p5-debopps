package DAO;

import Exceptions.DaoConfigurationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnector {

    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";

    public static Connection getConnection() throws SQLException, DaoConfigurationException {

        DaoSqlPropeties properties = new DaoSqlPropeties();
        String url = properties.getProperty(PROPERTY_URL, true);
        String driverClassName = properties.getProperty(PROPERTY_DRIVER, false);
        String password = properties.getProperty(PROPERTY_PASSWORD, false);
        String username = properties.getProperty(PROPERTY_USERNAME, password != null);

        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            throw new DaoConfigurationException(
                "Falta clase Driver '" + driverClassName + "' en el classpath.", e);
        }
        return DriverManager.getConnection(url, username, password);
    }
}
