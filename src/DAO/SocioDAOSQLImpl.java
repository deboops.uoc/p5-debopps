package DAO;

import Entidades.Socio;
import Exceptions.DaoConfigurationException;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SocioDAOSQLImpl implements SocioDAO {

    private Connection miConexion = null;

	@Override
	public List<Socio> listarTodos() throws DaoConfigurationException, SQLException {
        final String sql = "SELECT * FROM Socio";

        int codigo;
        List<Socio> lista = new ArrayList<>();
        String nombre, identificador, fechaAlta, fechaBaja;

        ResultSet rs = null;
		miConexion = JdbcConnector.getConnection();

		try {
            Statement sentencia = miConexion.createStatement();
			rs = sentencia.executeQuery(sql);

			while (rs.next()) {
				codigo = rs.getInt("codigo");
				nombre = rs.getString("nombre");
				identificador = rs.getString("identificador");
				fechaAlta = rs.getString("fechaAlta");
				fechaBaja = rs.getString("fechaBaja");
				Socio socio = new Socio(codigo, identificador, nombre, fechaAlta, fechaBaja);
				lista.add(socio);
			}
        } finally {
		    miConexion.close();
		}

		return lista;
	}

	@Override
	public Socio leerPorId(String id) throws DaoConfigurationException, SQLException {
        final String sql = "SELECT * FROM Socio where identificador = ?";

        int codigo;
        String nombre, identificador, fechaAlta, fechaBaja;

        Socio socio = null;
        ResultSet rs = null;
		miConexion = JdbcConnector.getConnection();

		try {
			PreparedStatement sentencia = miConexion.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			sentencia.setString(1, id);
			rs = sentencia.executeQuery();

			if (rs.next()) {

				codigo = rs.getInt("codigo");
				nombre = rs.getString("nombre");
				identificador = rs.getString("identificador");
				fechaAlta = rs.getString("fechaAlta");
				fechaBaja = rs.getString("fechaBaja");
				socio = new Socio(codigo, identificador, nombre, fechaAlta, fechaBaja);
			}
        } finally {
			miConexion.close();
		}

		return socio;
	}

	@Override
	public void registrar(Socio socio) throws DaoConfigurationException, SQLException {
        final String sql = "INSERT INTO Socio(identificador,nombre,fechaAlta,fechaBaja) VALUES(?,?,?,?)";

		miConexion = JdbcConnector.getConnection();
		miConexion.setAutoCommit(false);

		try {
			PreparedStatement sentencia = miConexion.prepareStatement(sql, Statement.NO_GENERATED_KEYS);

			sentencia.setString(1, socio.getIdentificador());
			sentencia.setString(2, socio.getNombre());
			sentencia.setString(3, socio.getFechaAlta());
			sentencia.setString(4, socio.getFechaBaja());

			sentencia.executeUpdate();
			miConexion.commit();

		} catch (SQLException err) {
            miConexion.rollback();
            throw err;
		} finally {
			miConexion.close();
		}
	}

	@Override
	public void modificar(Socio socio) throws DaoConfigurationException, SQLException {
        final String sql = "UPDATE Socio SET nombre=?, fechaAlta=?, fechaBaja=? WHERE identificador = ? ";

		miConexion = JdbcConnector.getConnection();
		miConexion.setAutoCommit(false);

		try {
			PreparedStatement sentencia = miConexion.prepareStatement(sql, Statement.NO_GENERATED_KEYS);
			sentencia.setString(1, socio.getNombre());
			sentencia.setString(2, socio.getFechaAlta());
			sentencia.setString(3, socio.getFechaBaja());
			sentencia.setString(4, socio.getIdentificador());

			sentencia.executeUpdate();
			miConexion.commit();

		} catch (SQLException err) {
            miConexion.rollback();
            throw err;
		} finally {
			miConexion.close();
		}
	}

	@Override
	public void eliminar(Socio socio) throws DaoConfigurationException, SQLException {
        final String sql = "DELETE FROM Socio WHERE identificador = ?";

		miConexion = JdbcConnector.getConnection();
		miConexion.setAutoCommit(false);

		try {
			PreparedStatement sentencia = miConexion.prepareStatement(sql, Statement.NO_GENERATED_KEYS);
			sentencia.setString(1, socio.getIdentificador());
			sentencia.executeUpdate();
			miConexion.commit();
        } catch (SQLException err) {
            miConexion.rollback();
            throw err;
        } finally {
            miConexion.close();
        }
	}
}
