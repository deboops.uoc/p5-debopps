package DAO;


import Exceptions.DaoConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DaoSqlPropeties {
    private static final String PROPERTIES_FILE = "daosql.properties";
    private static final Properties PROPERTIES = new Properties();

    static {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream archivoPropiedades = classLoader.getResourceAsStream(PROPERTIES_FILE);

        if (archivoPropiedades == null) {
            throw new DaoConfigurationException(
                    "Falta archivo de propiedades '" + PROPERTIES_FILE + "' en el classpath.");
        }

        try {
            PROPERTIES.load(archivoPropiedades);
        } catch (IOException e) {
            throw new DaoConfigurationException(
                    "No se puede cargar el archivo de propiedades '" + PROPERTIES_FILE + "'.", e);
        }
    }

    public void DAOProperties() throws DaoConfigurationException { }

    public String getProperty(String clave, boolean obligatorio) throws DaoConfigurationException {
        String propiedad = PROPERTIES.getProperty(clave);

        if (propiedad == null || propiedad.trim().length() == 0) {
            if (obligatorio) {
                throw new DaoConfigurationException("Propiedad requerida '" + clave + "'"
                        + " falta en el archivo de propiedades '" + PROPERTIES_FILE + "'.");
            } else {
                propiedad = null;
            }
        }

        return propiedad;
    }
}
