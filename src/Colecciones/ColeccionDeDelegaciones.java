package Colecciones;

import Entidades.Delegacion;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ColeccionDeDelegaciones {
	private List<Delegacion> listaDelegaciones = new ArrayList<>();

	public ColeccionDeDelegaciones(List<Delegacion> listaDelegaciones) {
		super();
		this.listaDelegaciones = listaDelegaciones;
	}

	public List<Delegacion> getListaDelegaciones() {
		return listaDelegaciones;
	}

 
    @XmlElement(name = "delegacion")
	public void setListaDelegaciones(List<Delegacion> listaDelegaciones) {
		this.listaDelegaciones = listaDelegaciones;
	}

	public ColeccionDeDelegaciones() {
		super();
	}


}
