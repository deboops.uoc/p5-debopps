package Colecciones;

import Entidades.Socio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ColeccionDeSocios {
	private List<Socio> listaSocios = new ArrayList<>();

	public ColeccionDeSocios(List<Socio> listaSocios) {
		super();
		this.listaSocios = listaSocios;
	}

	public List<Socio> getListasocios() {
		return listaSocios;
	}


	@XmlElement(name = "socio")
	public void setListasocios(List<Socio> listaSocios) {
		this.listaSocios = listaSocios;
	}

	public ColeccionDeSocios() {
		super();
	}

}
