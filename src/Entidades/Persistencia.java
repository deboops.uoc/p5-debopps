package Entidades;

import Enums.TipoPersistencia;

public final class Persistencia {
    public static TipoPersistencia persistenciaSelecionada;

    private Persistencia() {
        persistenciaSelecionada = TipoPersistencia.SQL;
    }

    public static void setPersistencia(TipoPersistencia persistencia) {
        persistenciaSelecionada = persistencia;
    }

    public static TipoPersistencia getPersistencia() {
        return persistenciaSelecionada;
    }
}
