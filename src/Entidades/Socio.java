package Entidades;

import java.time.*; // Este paquete contiene LocalDate, LocalTime y LocalDateTime.
import java.time.format.*;  // Este paquete contiene DateTimeFormatter.

public class Socio {

	private int codigo;
	private String identificador; // NIF, NIE, CIF
	private String nombre;
	private String fechaAlta;
	private String fechaBaja;
	private DateTimeFormatter patronFecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public Socio() {
		super();
	}

	public Socio(String identificador,  String nombre) {
		super();
		this.identificador = identificador;
		this.nombre = nombre;
		// Se genera fecha actual y se le aplica patrón
		this.fechaAlta = LocalDateTime.now().format(patronFecha);
		this.fechaBaja = null;
	}

	public Socio(int codigo, String identificador, String nombre, String fechaAlta, String fechaBaja) {
		super();
		this.codigo = codigo;
		this.identificador = identificador;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
		this.fechaBaja = fechaBaja;
	}

	public int getCodigo() {
		return codigo;
	}

	private void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			Socio socioAComparar = (Socio) obj;
			return identificador.equals(socioAComparar.identificador);
		}
		return false;

	}

	public int compareTo(Socio socioAComparar) {
		return this.identificador.compareTo(socioAComparar.identificador);
	}
	
	@Override
	public String toString() {
		return "Socio : identificador: " + identificador + ", nombre: " + nombre + ", fechaAlta: " + fechaAlta + ", fechaBaja: " + fechaBaja;
	}

	public void asignarFechaLocalAFechaBaja() {
		String fechaLocal = LocalDateTime.now().format(patronFecha);
		setFechaBaja(fechaLocal);
	}
	
}
