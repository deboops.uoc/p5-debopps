package Entidades;

public class Delegacion implements Comparable<Delegacion> {

	private int codigo;
	private String nombre; // El nombre de la delegación es clave única
	private String direccion;

	public Delegacion() {
		super();
	}

	public Delegacion(String nombre, String direccion) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			Delegacion delegacionAComparar = (Delegacion) obj;
			return nombre.equals(delegacionAComparar.nombre);
		}
		return false;

	}

	@Override
	public int compareTo(Delegacion delegacionAComparar) {
		return this.nombre.compareTo(delegacionAComparar.nombre);
	}

	@Override
	public String toString() {
		return "Delegacion : nombre: " + nombre + ", direccion: "	+ direccion;
	}

}