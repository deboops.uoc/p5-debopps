package Entidades;

import DAO.CRUD;

import Enums.TipoEntidades;
import Enums.TipoPersistencia;
import Exceptions.DaoConfigurationException;
import Factorias.DAOFactoria;
import java.sql.SQLException;
import java.util.ArrayList;
import Exceptions.NoExisteEntidadException;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"nombre", "cif", "delegaciones", "socios"})
public class SedeCentral {

	private int codigo;
	private String nombre;
	private String cif;
	private ArrayList<Delegacion> listaDelegaciones;
	private ArrayList<Socio> listaSocios;


	public SedeCentral() {
		super();
	}
	
	public SedeCentral(String nombre, String cif) {
		super();
		this.nombre = nombre;
		this.cif = cif;
		this.listaDelegaciones = new ArrayList<Delegacion>();
		this.listaSocios = new ArrayList<Socio>();
	}

	public SedeCentral(int idCodigo, String nombre, String cif) {
		this.codigo = idCodigo;
		this.nombre = nombre;
		this.cif = cif;
		this.listaDelegaciones = new ArrayList<Delegacion>();
		this.listaSocios = new ArrayList<Socio>();
	}

	public int getCodigo() {
		return codigo;
	}

	private void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public ArrayList<Delegacion> getDelegaciones() {
		return listaDelegaciones;
	}

	@XmlElementWrapper(name="delegaciones")
    @XmlElement(name = "delegacion")
	public void setDelegaciones(ArrayList<Delegacion> delegaciones) {
		this.listaDelegaciones = delegaciones;
	}

	public ArrayList<Socio> getSocios() {
		return listaSocios;
	}

	@XmlElementWrapper(name="socios")
    @XmlElement(name = "socio")
	public void setSocios(ArrayList<Socio> socios) {
		this.listaSocios = socios;
	}

	@Override
	public String toString() {
		return "SedeCentral : nombre=" + nombre + ", cif=" + cif ;
	}

	public void agregarSocio(Socio socio) {
		try {
			listaSocios.add(socio);
			actualizarSede();

		} catch (JAXBException | SQLException | DaoConfigurationException err) {
			System.out.println("Error - " + err.getMessage());
		}
	}

	public void actualizarSocio(Socio socio) {
		listaSocios.removeIf(soc -> soc.equals(socio));
		agregarSocio(socio);
	}

	public void eliminarSocio(Socio socio) {
		try {
			listaSocios.removeIf(soc -> soc.equals(socio));
			actualizarSede();

		} catch (JAXBException | NoExisteEntidadException | SQLException | DaoConfigurationException err) {
			System.out.println("Error - " + err.getMessage());
		}
	}

	public void agregarDelegacion(Delegacion delegacion) throws JAXBException, SQLException, DaoConfigurationException {
		try {
			this.listaDelegaciones.add(delegacion);
			actualizarSede();

		} catch (JAXBException | SQLException | DaoConfigurationException err) {
			System.out.println("Error - " + err.getMessage());
		}
	}

	public void actualizarDelegacion(Delegacion delegacion) throws DaoConfigurationException, JAXBException, SQLException {
		listaDelegaciones.removeIf(del -> del.equals(delegacion));
		agregarDelegacion(delegacion);
	}

	public void eliminarDelegacion(Delegacion delegacion) {
		try {
			listaDelegaciones.removeIf(del -> del.equals(delegacion));
			actualizarSede();

		} catch (JAXBException | SQLException | DaoConfigurationException err) {
			System.out.println("Error - " + err.getMessage());
		}
	}

	public void actualizarSede() throws JAXBException, SQLException, DaoConfigurationException {
		TipoPersistencia persistencia = Persistencia.getPersistencia();
		CRUD<SedeCentral> sedeDAO = DAOFactoria.getFactoria(TipoEntidades.SedeCentral, persistencia);
		sedeDAO.registrar(this);
	}
}
